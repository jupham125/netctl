package frontend

import (
	"testing"

	"gitlab.com/redfield/netctl/api"
)

func newTestWirelessFrontend() *WirelessFrontend {
	return &WirelessFrontend{
		publisher: newWirelessPropertiesPublisher(),
		props:     &api.WirelessProperties{},
	}
}

func TestUpdatePropsInvalidName(t *testing.T) {
	wf := newTestWirelessFrontend()

	key := "invalid-property"
	value := "value"

	err := wf.updateProperties(key, value, api.WirelessPropertiesUpdate_UNSPECIFIED)

	// Expect an error for invalid field name
	if err == nil {
		t.Errorf("expected error for setting invalid field name '%v'", key)
	}
}

func TestUpdatePropsTypeMismatch(t *testing.T) {
	wf := newTestWirelessFrontend()

	// There are no subscribers during this test,
	// don't publish updates
	utype := api.WirelessPropertiesUpdate_UNSPECIFIED

	table := map[string]interface{}{
		// Ifname should be string
		"Ifname": 0,
		// State should be WirelessState
		"State": "",
		// AccessPoints should be []*AccessPoint
		"AccessPoints": &api.InterfaceStats{},
		// Totally unsupported type
		"Stats": &api.WirelessProperties{},
	}

	for key, value := range table {
		err := wf.updateProperties(key, value, utype)
		if err == nil {
			t.Errorf("expected error for setting %T to %T", value, key)
		}
	}
}

// TestUpdatePropsValid tests that all expected key-value combinations, with respect to types,
// work.
func TestUpdatePropsValid(t *testing.T) {
	wf := newTestWirelessFrontend()

	// There are no subscribers during this test,
	// don't publish updates
	utype := api.WirelessPropertiesUpdate_UNSPECIFIED

	table := map[string]interface{}{
		// Test setting a string field
		"IfaceName": "wlan0",
		// Test setting WirelessState
		"State": api.WirelessState_CONNECTED,
		// Test setting *InterfaceStats
		"Stats": &api.InterfaceStats{},
		// Test setting []*AccessPoints
		"AccessPoints": make([]*api.AccessPoint, 0),
	}

	for key, value := range table {
		err := wf.updateProperties(key, value, utype)
		if err != nil {
			t.Errorf("unexpected error setting '%v' to valid value '%v': %v", key, value, err)
		}
	}
}
