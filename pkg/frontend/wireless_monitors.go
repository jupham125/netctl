// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package frontend

import (
	"context"
	"log"
	"reflect"
	"sync"
	"time"

	"github.com/pkg/errors"
	"github.com/rs/xid"

	"gitlab.com/redfield/netctl/api"
	"gitlab.com/redfield/netctl/pkg/wireless"
)

// wirelessPropertiesPublisher receives updates over one channel, and distributes
// the update to all of its subscribers.
type wirelessPropertiesPublisher struct {
	update chan api.WirelessPropertiesUpdate

	mux         sync.Mutex
	subscribers map[string]chan api.WirelessPropertiesUpdate
}

// wirelessPropertiesSubscriber subscribes to a publisher and listens for updates.
type wirelessPropertiesSubscriber struct {
	id     string
	update chan api.WirelessPropertiesUpdate
}

func newWirelessPropertiesPublisher() *wirelessPropertiesPublisher {
	p := &wirelessPropertiesPublisher{
		update:      make(chan api.WirelessPropertiesUpdate, 64),
		subscribers: make(map[string]chan api.WirelessPropertiesUpdate),
	}

	return p
}

func (p *wirelessPropertiesPublisher) subscribe() *wirelessPropertiesSubscriber {
	s := &wirelessPropertiesSubscriber{
		id:     xid.New().String(),
		update: make(chan api.WirelessPropertiesUpdate, 16),
	}

	p.mux.Lock()
	defer p.mux.Unlock()

	p.subscribers[s.id] = s.update

	return s
}

func (p *wirelessPropertiesPublisher) unsubscribe(s *wirelessPropertiesSubscriber) {
	p.mux.Lock()
	defer p.mux.Unlock()

	delete(p.subscribers, s.id)
}

func (p *wirelessPropertiesPublisher) publish(utype api.WirelessPropertiesUpdate_Type, props api.WirelessProperties) {
	// Ensure that caller does not block.
	go func() {
		update := api.WirelessPropertiesUpdate{
			Type:  utype,
			Props: &props,
		}

		p.update <- update
	}()
}

func (p *wirelessPropertiesPublisher) serve(ctx context.Context) {
	for {
		select {
		case <-ctx.Done():
			p.mux.Lock()

			for _, s := range p.subscribers {
				close(s)
			}

			p.mux.Unlock()

			return

		case u := <-p.update:
			p.mux.Lock()

			for _, s := range p.subscribers {
				s <- u
			}

			p.mux.Unlock()
		}
	}
}

// updateProperties will set value to key in the wireless properties, if possible. If utype is not UNSPECIFIED,
// the update will also be published. An error is returned if it is not possible to set value to the field
// specified by key, or if no such field exists.
func (wf *WirelessFrontend) updateProperties(key string, value interface{}, utype api.WirelessPropertiesUpdate_Type) error {
	wf.mux.Lock()
	defer wf.mux.Unlock()

	rfv := reflect.ValueOf(wf.props).Elem().FieldByName(key)
	rv := reflect.ValueOf(value)

	if !rfv.IsValid() {
		return errors.Errorf("no field with name '%v'", key)
	}

	if !rfv.CanSet() || !rfv.CanInterface() {
		return errors.Errorf("not allowed to set value of '%v'", key)
	}

	if rfv.Kind() != rv.Kind() {
		return errors.Errorf("cannot assign %v to %v", rv.Kind(), rfv.Kind())
	}

	// There are very few struct or array types that could be set here. Just
	// use a type switch to determine if this is valid.
	if rv.Kind() == reflect.Ptr || rv.Kind() == reflect.Slice || rv.Kind() == reflect.Array {
		switch value.(type) {
		case *api.InterfaceStats:
			if _, ok := rfv.Interface().(*api.InterfaceStats); !ok {
				return errors.Errorf("cannot assign %T to %T", value, rfv.Interface())
			}
		case []*api.AccessPoint:
			if _, ok := rfv.Interface().([]*api.AccessPoint); !ok {
				return errors.Errorf("cannot assign %T to %T", value, rfv.Interface())
			}
		default:
			return errors.Errorf("cannot assign unnsupported %T to %T", value, rfv.Interface())
		}
	}

	// Good to set the field now.
	rfv.Set(rv)

	if utype != api.WirelessPropertiesUpdate_UNSPECIFIED {
		wf.publisher.publish(utype, *wf.props)
	}

	return nil
}

func (wf *WirelessFrontend) monitorConnectionState() {
	convertState := map[wireless.State]api.WirelessState{
		wireless.Disconnected: api.WirelessState_DISCONNECTED,
		wireless.Connected:    api.WirelessState_CONNECTED,
		wireless.Connecting:   api.WirelessState_CONNECTING,
		wireless.Failed:       api.WirelessState_FAILED,
	}

	// Assume that state begins as 'Disconnected'
	prevState := api.WirelessState_UNKNOWN

	sc := make(chan wireless.State)
	go wf.manager.WatchState(sc)

	for {
		var state api.WirelessState

		select {
		case s := <-sc:
			state = convertState[s]

		case <-wf.ctx.Done():
			return

		default:
			time.Sleep(1 * time.Second)
			state = convertState[wf.manager.State()]
		}

		var portalURL string

		if state == api.WirelessState_CONNECTED && prevState != api.WirelessState_CONNECTED {
			// State changed to Connected -- check for portal
			if pres, url := wf.manager.Interface().CheckForPortal(); pres {
				state = api.WirelessState_PORTAL
				portalURL = url
			}
		}

		if state != prevState {
			// If there is not a portal, this will make sure the connection
			// property is set to empty
			err := wf.updateProperties("PortalUrl", portalURL, api.WirelessPropertiesUpdate_UNSPECIFIED)
			if err != nil {
				log.Printf("Failed to update portal URL: %v", err)
			}

			err = wf.updateProperties("State", state, api.WirelessPropertiesUpdate_STATE)
			if err != nil {
				log.Printf("Failed to update state: %v", err)
			}
		}

		// Keep track of the state
		prevState = state
	}
}

func (wf *WirelessFrontend) monitorIPAddress() {
	var prevIP string

	for {
		select {
		case <-wf.ctx.Done():
			return

		default:
			time.Sleep(1 * time.Second)

			if ip := wf.manager.IPAddress(); ip != prevIP {
				err := wf.updateProperties("Ssid", wf.manager.CurrentSSID(), api.WirelessPropertiesUpdate_UNSPECIFIED)
				if err != nil {
					log.Printf("Failed to update SSID: %v", err)
				}

				err = wf.updateProperties("IpAddress", ip, api.WirelessPropertiesUpdate_IP)
				if err != nil {
					log.Printf("Failed to update IP address: %v", err)
				}

				// Keep track of the new IP
				prevIP = ip
			}
		}
	}
}

func (wf *WirelessFrontend) monitorSignalStrength() {
	var prevSignal string

	for {
		select {
		case <-wf.ctx.Done():
			return

		default:
			time.Sleep(1 * time.Second)

			if signal := wf.manager.SignalStrength(); signal != prevSignal {
				err := wf.updateProperties("SignalStrength", signal, api.WirelessPropertiesUpdate_SIGNAL)
				if err != nil {
					log.Printf("Failed to update signal strength: %v", err)
				}

				// Keep track of the new IP
				prevSignal = signal
			}
		}
	}
}

func (wf *WirelessFrontend) monitorAvailableNetworks() {
	for {
		select {
		case <-wf.ctx.Done():
			return

		default:
			var aps []*api.AccessPoint

			for _, v := range wf.manager.AvailableNetworks() {
				ap := api.AccessPoint{
					Ssid:           v.SSID,
					SignalStrength: v.SignalStrength.String(),
					KeyMgmt:        v.KeyManagement[0],
				}

				aps = append(aps, &ap)
			}

			err := wf.updateProperties("AccessPoints", aps, api.WirelessPropertiesUpdate_ACCESSPOINTS)
			if err != nil {
				log.Printf("Failed to update access points: %v", err)
			}

			time.Sleep(5 * time.Second)
		}
	}
}

func (wf *WirelessFrontend) monitorInterfaceStats() {
	for {
		select {
		case <-wf.ctx.Done():
			return

		default:
			s, err := wf.manager.Interface().Stats()
			if err != nil {
				log.Printf("could not get stats for interface '%v': %v", wf.manager.Interface().Name, err)

				// If there was an error getting the stats, don't send an update
				time.Sleep(5 * time.Second)
				continue
			}

			stats := &api.InterfaceStats{
				RxBytes:   s.RxBytes,
				RxErrors:  s.RxErrors,
				RxDropped: s.RxDropped,
				RxPackets: s.RxPackets,
				TxBytes:   s.TxBytes,
				TxErrors:  s.TxErrors,
				TxDropped: s.TxDropped,
				TxPackets: s.TxPackets,
			}

			err = wf.updateProperties("Stats", stats, api.WirelessPropertiesUpdate_STATS)
			if err != nil {
				log.Printf("Failed to update interface stats: %v", err)
			}

			time.Sleep(5 * time.Second)
		}
	}
}

func (wf *WirelessFrontend) startMonitors() {
	go wf.publisher.serve(wf.ctx)

	go wf.monitorConnectionState()
	go wf.monitorIPAddress()
	go wf.monitorSignalStrength()
	go wf.monitorAvailableNetworks()
	go wf.monitorInterfaceStats()
}
