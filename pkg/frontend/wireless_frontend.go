// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package frontend

import (
	"context"
	"log"
	"net"
	"os"
	"sync"
	"time"

	"github.com/pkg/errors"
	"google.golang.org/grpc"

	"gitlab.com/redfield/netctl/api"
	"gitlab.com/redfield/netctl/pkg/wireless"
)

// WirelessFrontend is a netctl frontend that is linked to a wireless interface.
type WirelessFrontend struct {
	*frontend

	// Wireless interface name and manager
	ifname  string
	manager *wireless.Manager

	// Properties and configuration, protected by lock.
	mux   sync.Mutex
	props *api.WirelessProperties
	conf  *api.WirelessNetworkConfiguration

	// Properties update publisher.
	publisher *wirelessPropertiesPublisher

	// Wireless options, e.g. periodic scan, networking caching, etc.
	*wirelessOptions
}

// NewWirelessFrontend returns a new netctl frontend for a wireless interface.
func NewWirelessFrontend(ifname, addr string, opts ...WirelessOption) (*WirelessFrontend, error) {
	ti, err := api.ParseTransport(addr)
	if err != nil {
		return nil, errors.Wrap(err, "invalid address")
	}

	manager, err := wireless.NewManager(ifname)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create wireless manager")
	}

	ctx, cancel := context.WithCancel(context.Background())
	frontend := &frontend{
		// Default the uuid to be the interface name.
		// This can be overridden using the WithUUID
		// option.
		uuid:        ifname,
		typ:         api.InterfaceType_WIRELESS,
		ti:          ti,
		init:        &initializer{},
		initSuccess: make(chan bool, 1),
		ctx:         ctx,
		cancel:      cancel,
	}

	wf := &WirelessFrontend{
		frontend:        frontend,
		ifname:          ifname,
		manager:         manager,
		props:           &api.WirelessProperties{},
		conf:            &api.WirelessNetworkConfiguration{},
		publisher:       newWirelessPropertiesPublisher(),
		wirelessOptions: &wirelessOptions{},
	}

	for _, opt := range opts {
		opt.apply(wf.wirelessOptions)
	}

	if wf.doAssignUUID {
		uuid, err := hashInterfaceNameWithUUID(ifname)
		if err != nil {
			return nil, errors.Wrap(err, "failed to generate uuid for frontend")
		}
		wf.uuid = uuid
	}

	return wf, nil
}

// Serve starts the wireless frontend service.
func (wf *WirelessFrontend) Serve() error {
	lis, err := wf.newListener()
	if err != nil {
		return errors.Wrap(err, "failed to create listener")
	}
	defer lis.Close()

	wf.startMonitors()

	wf.server = grpc.NewServer()

	api.RegisterNetctlFrontServer(wf.server, wf)

	// Trigger postInitialization goroutine.
	go wf.postInitialization()

	return wf.server.Serve(lis)
}

func (wf *WirelessFrontend) newListener() (net.Listener, error) {
	switch wf.ti.Network() {
	case "tcp":
		return net.Listen(wf.ti.Network(), wf.ti.String())
	case "unix":
		lis, err := net.Listen(wf.ti.Network(), wf.ti.String())
		if err != nil {
			return lis, err
		}

		// Set permissions of the socket so that is world-writable.
		if err := os.Chmod(wf.ti.String(), 0777); err != nil {
			lis.Close()

			return nil, errors.Wrap(err, "failed to create socket listener")
		}

		return lis, nil
	default:
		return nil, errors.Errorf("unknown network type %s", wf.ti.Network())
	}
}

// Close tears down the WirelessFrontend
func (wf *WirelessFrontend) Close() error {
	return wf.teardown()
}

// Initialize starts the initialization phase and performs startup actions.
func (wf *WirelessFrontend) Initialize(nb *api.BackendMetaData, timeout time.Duration) error {
	err := wf.initialize(nb, timeout)
	if err != nil {
		return err
	}

	wf.initSuccess <- true

	return nil
}

// postInitialization performs any actions that should happen as
// a part of service startup, but need to wait until the frontend
// has initialized with the backend.
func (wf *WirelessFrontend) postInitialization() {
	select {
	case <-wf.ctx.Done():
		return
	case <-wf.initSuccess:
		break
	}

	go wf.startScan()
	go wf.connectToLastNetwork()
}

// setConf sets the frontends network configuration.
func (wf *WirelessFrontend) setConf(conf *api.WirelessNetworkConfiguration) {
	wf.mux.Lock()
	defer wf.mux.Unlock()

	wf.conf = conf
}

func (wf *WirelessFrontend) getProps() api.WirelessProperties {
	wf.mux.Lock()
	defer wf.mux.Unlock()

	return *wf.props
}

// WirelessOption is used to configure a netctl frontend.
type WirelessOption interface {
	apply(*wirelessOptions)
}

type wirelessOptions struct {
	// Indicates if the wireless frontend should try to connect to
	// most-recently used known network on startup. If there are
	// none in present in the scan results, nothing happens.
	doConnectLastNetwork bool

	// Indicates if the frontend should cache network configurations,
	// and re-use them on connect.
	doRememberNetworks bool

	// Indicates if the frontend should perform periodic AP scans.
	doPeriodicScan bool

	// Indicates that a proper UUID should be assigned to the frontend,
	// rather than indentifying it by its interface name.
	doAssignUUID bool
}

type funcWirelessOption struct {
	f func(*wirelessOptions)
}

func (fwo *funcWirelessOption) apply(w *wirelessOptions) {
	fwo.f(w)
}

func newFuncWirelessOption(f func(*wirelessOptions)) *funcWirelessOption {
	return &funcWirelessOption{f}
}

// WithConnectLastNetwork tells the Service to try to connect to the last
// known network on startup.
func WithConnectLastNetwork() WirelessOption {
	return newFuncWirelessOption(func(w *wirelessOptions) {
		w.doConnectLastNetwork = true
	})
}

// WithRememberNetworks tells the Service to cache network configurations.
func WithRememberNetworks() WirelessOption {
	return newFuncWirelessOption(func(w *wirelessOptions) {
		w.doRememberNetworks = true
	})
}

// WithPeriodicScan tells the service to periodically scan for wireless networks.
func WithPeriodicScan() WirelessOption {
	return newFuncWirelessOption(func(w *wirelessOptions) {
		w.doPeriodicScan = true
	})
}

// WithUUID tells the frontend to use a UUID as its identifier with the backend.
// By default, the frontend is identified by its interface name.
func WithUUID() WirelessOption {
	return newFuncWirelessOption(func(w *wirelessOptions) {
		w.doAssignUUID = true
	})
}

func (wf *WirelessFrontend) connectToLastNetwork() {
	if !wf.doConnectLastNetwork {
		return
	}

	nf := api.FrontendMetaData{
		Uuid: wf.uuid,
	}

	networks, err := wf.bc.GetSavedNetworks(&nf)
	if err != nil {
		log.Print(errors.Wrap(err, "failed to get saved networks"))
		return
	}

	// Scan may not be finished yet, wait a little bit for a populated list.
	avail := wf.manager.AvailableNetworks()
	for start := time.Now(); time.Since(start) < 20*time.Second; {
		if len(avail) > 0 {
			break
		}

		avail = wf.manager.AvailableNetworks()
	}

	// Go through the saved networks in order. If there is a match with the scan
	// results, connect to that network with the cached configuration.
	for _, n := range networks {
		for _, a := range avail {
			if n.GetSsid() != a.SSID {
				continue
			}

			if err := wf.manager.Connect(formatNetworkConfiguration(n)); err != nil {
				log.Print(errors.Wrap(err, "failed to connect to saved network"))
				return
			}
		}
	}
}

func (wf *WirelessFrontend) startScan() {
	var scanInterval time.Duration

	// If periodic scan is disabled, return after first scan
	if !wf.doPeriodicScan {
		if err := wf.manager.Scan(); err != nil {
			log.Printf("unable to perform AP scan: %v", err)
		}

		return
	}

	for {
		select {
		case <-wf.ctx.Done():
			return

		default:
			if err := wf.manager.Scan(); err != nil {
				log.Printf("unable to perform AP scan: %v", err)
			}

			// Scan more frequently when interface is disconnected
			if wf.manager.State() == wireless.Disconnected {
				scanInterval = 20 * time.Second
			} else {
				scanInterval = 120 * time.Second
			}

			// Sleep until next scan
			time.Sleep(scanInterval)
		}
	}
}

// Notify is used to notify a frontend of certain events such as initialization ACK's, and a backend going down.
func (wf *WirelessFrontend) Notify(ctx context.Context, r *api.NotifyRequest) (*api.NotifyReply, error) {
	if err := wf.handleNotify(r.GetNotification()); err != nil {
		return nil, err
	}

	return &api.NotifyReply{}, nil
}

// WirelessConnect connects to a wireless network.
func (wf *WirelessFrontend) WirelessConnect(ctx context.Context, r *api.WirelessConnectRequest) (*api.WirelessConnectReply, error) {
	conf := r.GetConfig()
	if conf == nil {
		return nil, errors.New("received empty configuration")
	}

	s, err := wf.handleConnect(conf)
	if err != nil {
		return nil, err
	}

	return &api.WirelessConnectReply{State: s}, nil
}

func (wf *WirelessFrontend) handleConnect(conf *api.WirelessNetworkConfiguration) (api.WirelessState, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	state := make(chan api.WirelessState)
	go func() {
		state <- wf.waitForConnectionResult(ctx)
	}()

	err := wf.manager.Connect(formatNetworkConfiguration(conf))
	if err != nil {
		return api.WirelessState_UNKNOWN, errors.Wrap(err, "wireless manager failed connection attempt")
	}

	s := <-state

	if s == api.WirelessState_UNKNOWN {
		return s, errors.New("connection attempt timed out")
	}

	if s == api.WirelessState_CONNECTED {
		wf.saveNetworkConfiguration(conf)
	}

	return s, nil
}

func (wf *WirelessFrontend) waitForConnectionResult(ctx context.Context) api.WirelessState {
	sub := wf.publisher.subscribe()
	defer wf.publisher.unsubscribe(sub)

	for {
		select {
		case <-ctx.Done():
			return api.WirelessState_UNKNOWN

		case update := <-sub.update:
			if update.Type != api.WirelessPropertiesUpdate_STATE {
				continue
			}

			switch state := update.Props.State; state {
			case api.WirelessState_CONNECTED, api.WirelessState_FAILED, api.WirelessState_PORTAL:
				return state
			default:
				continue
			}
		}
	}
}

func (wf *WirelessFrontend) saveNetworkConfiguration(conf *api.WirelessNetworkConfiguration) {
	if wf.doRememberNetworks {
		f := &api.FrontendMetaData{
			Uuid: wf.uuid,
		}

		err := wf.bc.SaveNetwork(f, conf)
		if err != nil {
			log.Printf("Failed to save network configuration for '%v'", conf.Ssid)
		}
	}

	wf.setConf(conf)
}

// WirelessDisconnect disconnects a wireless network.
func (wf *WirelessFrontend) WirelessDisconnect(ctx context.Context, r *api.WirelessDisconnectRequest) (*api.WirelessDisconnectReply, error) {
	err := wf.handleDisconnect()
	if err != nil {
		return nil, err
	}

	return &api.WirelessDisconnectReply{}, nil
}

func (wf *WirelessFrontend) handleDisconnect() error {
	err := wf.manager.Disconnect()
	if err != nil {
		return errors.Wrap(err, "wireless manager failed disconnect")
	}

	wf.setConf(&api.WirelessNetworkConfiguration{})

	return nil
}

// WirelessMonitorProperties handles a stream of wireless properties updates.
func (wf *WirelessFrontend) WirelessMonitorProperties(r *api.WirelessMonitorPropertiesRequest, stream api.NetctlFront_WirelessMonitorPropertiesServer) error {
	sub := wf.publisher.subscribe()
	defer wf.publisher.unsubscribe(sub)

	for update := range sub.update {
		// Make a copy of the update, do not pass a reference
		// of the loop variable.
		u := update

		r := &api.WirelessMonitorPropertiesReply{Update: &u}

		if err := stream.Send(r); err != nil {
			return errors.Wrap(err, "failed to send wireless properties update")
		}
	}

	return nil
}

// WirelessGetProperties returns the current properties of the wireless frontend.
func (wf *WirelessFrontend) WirelessGetProperties(ctx context.Context, r *api.WirelessGetPropertiesRequest) (*api.WirelessGetPropertiesReply, error) {
	props := wf.getProps()

	return &api.WirelessGetPropertiesReply{Props: &props}, nil
}
