// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package supplicant

import (
	"bytes"
	"fmt"
	"reflect"

	"github.com/godbus/dbus"
)

const (
	// wpa_supplicant DBus destination
	wpaDBusDestination = "fi.w1.wpa_supplicant1"

	// wpa_supplicant base object path
	wpaBaseObjectPath = "/fi/w1/wpa_supplicant1"

	// wpa_supplicant Interfaces property
	wpaInterfaces = "fi.w1.wpa_supplicant1.Interfaces"

	// wpa_supplicant methods
	wpaGetInterface = "fi.w1.wpa_supplicant1.GetInterface"

	// wpa_supplicant Interface methods
	wpaInterface         = "fi.w1.wpa_supplicant1.Interface"
	wpaAddNetwork        = "fi.w1.wpa_supplicant1.Interface.AddNetwork"
	wpaBSSs              = "fi.w1.wpa_supplicant1.Interface.BSSs"
	wpaCurrentBSS        = "fi.w1.wpa_supplicant1.Interface.CurrentBSS"
	wpaDisconnect        = "fi.w1.wpa_supplicant1.Interface.Disconnect"
	wpaIfname            = "fi.w1.wpa_supplicant1.Interface.Ifname"
	wpaRemoveNetwork     = "fi.w1.wpa_supplicant1.Interface.RemoveNetwork"
	wpaRemoveAllNetworks = "fi.w1.wpa_supplicant1.Interface.RemoveAllNetworks"
	wpaScan              = "fi.w1.wpa_supplicant1.Interface.Scan"
	wpaSelectNetwork     = "fi.w1.wpa_supplicant1.Interface.SelectNetwork"

	// wpa_supplicant Interface properties
	wpaCurrentNetwork = "fi.w1.wpa_supplicant1.Interface.CurrentNetwork"
	wpaState          = "fi.w1.wpa_supplicant1.Interface.State"

	// wpa_supplicant Network properties
	wpaNetworkProperties = "fi.w1.wpa_supplicant1.Network.Properties"

	// wpa_supplicant BSS properties
	wpaBSSRSN    = "fi.w1.wpa_supplicant1.BSS.RSN"
	wpaBSSSignal = "fi.w1.wpa_supplicant1.BSS.Signal"

	// The golangci-lint tool reports a false positive on
	// 'Potential hardcoded credentials' here. Tell it to ignore
	// this as it is just a DBus endpoint.
	// #nosec
	wpaSSID = "fi.w1.wpa_supplicant1.BSS.SSID"
)

// wpa_supplicant interface states
const (
	StateDisconnected   = "disconnected"
	StateInactive       = "inactive"
	StateScanning       = "scanning"
	StateAuthenticating = "authenticating"
	StateAssociating    = "associating"
	StateAssociated     = "associated"
	State4WayHandshake  = "4way_handshake"
	StateGroupHandshake = "group_handshake"
	StateCompleted      = "completed"
	StateUnknown        = "unknown"
)

// wpa_supplicant key management options
const (
	AuthOpen = "NONE"
	AuthPSK  = "WPA-PSK"
)

// NetworkConfiguration represents a network configuration as described
// in wpa_supplicant.conf(5)
type NetworkConfiguration struct {
	SSID          string `conf:"ssid"`
	PSK           string `conf:"psk"`
	KeyManagement string `conf:"key_mgmt"`
	Identity      string `conf:"identity"`
	Password      string `conf:"password"`
	EAP           string `conf:"eap"`
	CaCert        string `conf:"ca_cert"`
	ClientCert    string `conf:"client_cert"`
	PrivKey       string `conf:"private_key"`
	PrivKeyPasswd string `conf:"private_key_passwd"`
	ScanSSID      int32  `conf:"scan_ssid"`
}

// Create config map needed by DBus out of NetworkConfiguration type
func networkConfigurationAsMap(c NetworkConfiguration) map[string]interface{} {
	config := make(map[string]interface{})

	// Use reflect to get fields values and keys
	t := reflect.TypeOf(c)
	for i := 0; i < t.NumField(); i++ {
		// Use the field tag to get map key
		key := t.Field(i).Tag.Get("conf")

		// Get the value of the field
		v := reflect.ValueOf(&c).Elem()
		value := v.Field(i).Interface()

		// Only set in the config if 'value' is not its type's zero value
		if value != reflect.Zero(reflect.TypeOf(value)).Interface() {
			config[key] = value
		}
	}

	return config
}

// Supplicant provides a mechanism to interface with
// the wpa_supplicant DBus API.
type Supplicant struct {
	bus *dbus.Conn // DBus connection

	iface dbus.ObjectPath // Object path for network interface
}

// SignalStrength represents the strength of a signal in -dBm.
type SignalStrength int16

// String returns a string description of SignalStrength.
func (s SignalStrength) String() string {
	// Signal strength is represented in -dBm (0 to -100)
	// The closer to 0, the better the signal
	switch {
	case s > 0:
		// BSSSignalStrength returns 1 on error
		return ""
	case s >= -30:
		return "Excellent"
	case s >= -67:
		return "Very Good"
	case s >= -70:
		return "Fair"
	case s >= -80:
		return "Weak"
	case s >= -90:
		return "Very Weak"
	}

	return "Unknown"
}

// ScanResult contains information about an available AP.
type ScanResult struct {
	SSID           string         // Name of the network
	KeyManagement  []string       // Key management suite
	SignalStrength SignalStrength // Signal strength of the BSS
}

// NewSupplicant returns a Supplicant associated with a wireless interface
// specified by ifname.
func NewSupplicant(ifname string) (*Supplicant, error) {
	// Connect to system bus
	bus, err := dbus.SystemBus()
	if err != nil {
		return nil, err
	}

	var iface dbus.ObjectPath
	obj := bus.Object(wpaDBusDestination, wpaBaseObjectPath)
	err = obj.Call(wpaGetInterface, 0, ifname).Store(&iface)
	if err != nil {
		return nil, err
	}

	s := Supplicant{
		bus:   bus,
		iface: iface,
	}

	return &s, nil
}

// CurrentBSS returns an object path to the BSS currently associated
// with the supplicant.
func (s *Supplicant) CurrentBSS() (dbus.ObjectPath, error) {
	var bss dbus.ObjectPath

	obj := s.bus.Object(wpaDBusDestination, s.iface)
	result, err := obj.GetProperty(wpaCurrentBSS)
	if err != nil {
		return bss, err
	}
	bss = result.Value().(dbus.ObjectPath)

	return bss, nil
}

// AvailableBSSList gives a list of available BSSs.
func (s *Supplicant) AvailableBSSList() ([]dbus.ObjectPath, error) {
	var bss []dbus.ObjectPath

	obj := s.bus.Object(wpaDBusDestination, s.iface)
	result, err := obj.GetProperty(wpaBSSs)
	if err != nil {
		return bss, err
	}
	bss = result.Value().([]dbus.ObjectPath)

	return bss, nil
}

// CurrentNetwork returns an object path to the network currently associated
// with the supplicant.
func (s *Supplicant) CurrentNetwork() (dbus.ObjectPath, error) {
	var network dbus.ObjectPath

	obj := s.bus.Object(wpaDBusDestination, s.iface)
	result, err := obj.GetProperty(wpaCurrentNetwork)
	if err != nil {
		return network, err
	}
	// Use type assertion on the Variant's value
	network = result.Value().(dbus.ObjectPath)

	return network, nil
}

// AddNetwork adds a network that the supplicant can associate with.
func (s *Supplicant) AddNetwork(c NetworkConfiguration) (dbus.ObjectPath, error) {
	// Convert the NetworkConfiguration to a map
	config := networkConfigurationAsMap(c)

	obj := s.bus.Object(wpaDBusDestination, s.iface)

	var result dbus.ObjectPath
	err := obj.Call(wpaAddNetwork, 0, config).Store(&result)

	if err != nil {
		return result, err
	}

	return result, nil
}

// NetworkDisconnect disconnects the supplicants network interface from
// the current network.
func (s *Supplicant) NetworkDisconnect() error {
	obj := s.bus.Object(wpaDBusDestination, s.iface)
	c := obj.Call(wpaDisconnect, 0)

	// If all goes well this will return nil
	return c.Err
}

// SelectNetwork associates the supplicant with a configured network. Use
// AddNetwork to configure a network.
func (s *Supplicant) SelectNetwork(network dbus.ObjectPath) error {
	obj := s.bus.Object(wpaDBusDestination, s.iface)
	c := obj.Call(wpaSelectNetwork, 0, network)

	// If all goes well this will return nil
	return c.Err
}

// RemoveNetwork removes the network from the interface.
func (s *Supplicant) RemoveNetwork(network dbus.ObjectPath) error {
	obj := s.bus.Object(wpaDBusDestination, s.iface)
	c := obj.Call(wpaRemoveNetwork, 0, network)

	// If all goes well this will return nil
	return c.Err
}

// RemoveAllNetworks removes all configured networks from the interface.
func (s *Supplicant) RemoveAllNetworks() error {
	obj := s.bus.Object(wpaDBusDestination, s.iface)
	c := obj.Call(wpaRemoveAllNetworks, 0)

	// If all goes well this will return nil
	return c.Err
}

// InterfaceState returns the connection state of the network interface.
func (s *Supplicant) InterfaceState() string {
	obj := s.bus.Object(wpaDBusDestination, s.iface)
	result, err := obj.GetProperty(wpaState)
	if err != nil {
		return ""
	}

	// Assert that the underlying value is string
	return result.Value().(string)
}

// PropertiesChanged returns a channel registered to listen for
// the PropertiesChanged signal
func (s *Supplicant) PropertiesChanged() (chan *dbus.Signal, error) {
	arg := "type='signal',path='%v',interface='%v',member='PropertiesChanged'"
	arg = fmt.Sprintf(arg, s.iface, wpaInterface)

	call := s.bus.BusObject().Call("org.freedesktop.DBus.AddMatch", 0, arg)
	if err := call.Err; err != nil {
		return nil, err
	}

	c := make(chan *dbus.Signal, 10)
	s.bus.Signal(c)

	return c, nil
}

// NetworkProperties returns properties about the currently associated
// network.
func (s *Supplicant) NetworkProperties() map[string]dbus.Variant {
	var props map[string]dbus.Variant

	network, err := s.CurrentNetwork()

	// If there is no network, don't make the dbus call
	if err != nil || network == "/" {
		return props
	}

	obj := s.bus.Object(wpaDBusDestination, network)
	result, err := obj.GetProperty(wpaNetworkProperties)
	if err != nil {
		return props
	}
	// DBus DICTs are encoded as Go maps
	props = result.Value().(map[string]dbus.Variant)

	return props
}

// CurrentSignalStrength returns the signal strength of the current BSS.
func (s *Supplicant) CurrentSignalStrength() SignalStrength {
	bss, _ := s.CurrentBSS()

	// If there is no BSS, don't make the dbus call
	if bss == "/" {
		return 1
	}

	obj := s.bus.Object(wpaDBusDestination, bss)
	result, err := obj.GetProperty(wpaBSSSignal)
	if err != nil {
		// Signal strength is represented in -dBm (0 to -100)
		// On error, return positive number
		return 1
	}

	// DBus type 'n' converts to int16
	return SignalStrength(result.Value().(int16))
}

// ScanResults returns the results of the last scan.
func (s *Supplicant) ScanResults() []ScanResult {
	sr := make([]ScanResult, 0)
	availBSSs, err := s.AvailableBSSList()
	if err != nil {
		return sr
	}

	for _, bss := range availBSSs {
		obj := s.bus.Object(wpaDBusDestination, bss)

		// Get the SSID property of the BSS
		result, err := obj.GetProperty(wpaSSID)
		if err != nil {
			// Continue to the next BSS
			continue
		}

		ssid := result.Value().([]byte)

		// Sometimes a null byte array is given, which causes issues
		// down the line. We are going to assume this is meant to
		// represent a hidden network, so we will replace this with
		// an empty string.
		if bytes.Equal(ssid, make([]byte, len(ssid))) {
			ssid = []byte("")
		}

		// Get the Signal property of the BSS
		result, err = obj.GetProperty(wpaBSSSignal)
		if err != nil {
			// Continue to the next BSS
			continue
		}
		// DBus type 'n' converts to int16
		signal := SignalStrength(result.Value().(int16))

		// Get the key management suite
		result, err = obj.GetProperty(wpaBSSRSN)
		if err != nil {
			// Continue to the next BSS
			continue
		}
		rsnInfo := result.Value().(map[string]dbus.Variant)

		var keyMgmt []string
		if v, ok := rsnInfo["KeyMgmt"]; ok {
			// v is a DBus variant, assert string
			keyMgmt = v.Value().([]string)
		}

		if len(keyMgmt) == 0 {
			keyMgmt = append(keyMgmt, "unknown")
		}

		// Append to results
		sr = append(sr, ScanResult{string(ssid), keyMgmt, signal})
	}

	return uniqueSSIDs(sr)
}

// Scan performs an AP scan.
func (s *Supplicant) Scan() error {
	// Args for wpa_supplicant scan
	args := make(map[string]interface{})

	args["Type"] = "active"
	args["AllowRoam"] = false

	obj := s.bus.Object(wpaDBusDestination, s.iface)
	c := obj.Call(wpaScan, 0, args)

	// If all goes well this will return nil
	return c.Err
}

// WirelessInterfaceNames returns a list of wireless interface names known to
// wpa_supplicant.
func WirelessInterfaceNames() ([]string, error) {
	names := make([]string, 0)

	bus, err := dbus.SystemBus()
	if err != nil {
		return names, err
	}

	obj := bus.Object(wpaDBusDestination, wpaBaseObjectPath)
	result, err := obj.GetProperty(wpaInterfaces)
	if err != nil {
		return names, err
	}

	ifaces := result.Value().([]dbus.ObjectPath)
	for _, v := range ifaces {
		// v is an object path to an interface
		obj = bus.Object(wpaDBusDestination, v)
		result, err = obj.GetProperty(wpaIfname)
		if err != nil {
			// Continue to next interface
			continue
		}
		names = append(names, result.Value().(string))
	}

	return names, nil
}

// Remove duplicate SSIDs from list
func uniqueSSIDs(results []ScanResult) []ScanResult {
	keys := make(map[string]bool)
	list := []ScanResult{}

	for _, r := range results {
		if _, value := keys[r.SSID]; !value {
			keys[r.SSID] = true
			list = append(list, r)
		}
	}

	return list
}
