// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package wireless

import (
	"log"
	"sync"
	"time"

	"github.com/godbus/dbus"
	"github.com/pkg/errors"
	"github.com/rs/xid"

	"gitlab.com/redfield/netctl/pkg/supplicant"
)

// State represents a connection state
type State int32

// Possible values of type State:
// 	Disconnected
// 	Connected
// 	Connecting
// 	Failed
const (
	Disconnected State = iota
	Connected
	Connecting
	Failed
)

// String returns the string represetation of the state.
func (s State) String() string {
	switch s {
	case Disconnected:
		return "disconnected"
	case Connected:
		return "connected"
	case Connecting:
		return "connecting"
	case Failed:
		return "failed"
	}

	return "unknown"
}

// Manager is used to manage a wireless network interface.
type Manager struct {
	supplicant *supplicant.Supplicant

	iface *Interface

	smux  sync.Mutex
	state State

	// wpa_supplicant does not have states that represent a
	// disconnecting interface, as they only describe the association
	// and handshake protocols.
	//
	// We don't want to try to track a disconnecting state in the state
	// handler, but we need to be able to tell the state handler when
	// a disconnect event is triggered to prevent false states.
	dmux          sync.Mutex
	disconnecting bool

	stateBroadcaster *broadcaster
}

// NewManager returns a Manager for a specified wireless
// interface.
func NewManager(ifname string) (*Manager, error) {
	s, err := supplicant.NewSupplicant(ifname)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create supplicant")
	}

	iface, err := NewInterface(ifname)
	if err != nil {
		return nil, errors.Wrap(err, "failed to initialize interface")
	}

	m := Manager{
		supplicant:       s,
		iface:            iface,
		state:            Disconnected,
		stateBroadcaster: newBroadcaster(),
	}

	go m.interfaceStateListener()
	go m.stateBroadcaster.serve()

	return &m, nil
}

// Interface returns the Interface associated with the Manager.
func (m *Manager) Interface() *Interface {
	return m.iface
}

// Connect connects to a network using a specified configuration.
func (m *Manager) Connect(conf supplicant.NetworkConfiguration) error {
	// Make sure we are in a good state to begin an association attempt
	_ = m.Disconnect()

	obj, err := m.supplicant.AddNetwork(conf)
	if err != nil {
		return err
	}

	err = m.supplicant.SelectNetwork(obj)
	if err != nil {
		return err
	}

	return nil
}

// Disconnect disconnects the interface from the current network.
func (m *Manager) Disconnect() error {
	m.setDisconnecting(true)
	defer m.setDisconnecting(false)

	l := m.stateBroadcaster.registerListener()
	defer m.stateBroadcaster.unregisterListener(l)

	// If the state is already disconnected, don't do anything.
	if m.State() == Disconnected {
		return nil
	}

	done := make(chan bool)
	go func() {
		for state := range l.update {
			if state == Disconnected {
				done <- true
				return
			}
		}
	}()

	err := m.disconnectAndResetNetworks()

	<-done

	return err
}

// disconnectAndResetNetworks is used to reset the connection state
// to a known, 'Disconnected' state.
func (m *Manager) disconnectAndResetNetworks() error {
	// This may fail because the interface is not actually
	// connected. But, that error is not useful and we still
	// want to remove the configured networks.
	_ = m.supplicant.NetworkDisconnect()

	err := m.supplicant.RemoveAllNetworks()
	if err != nil {
		return errors.Wrap(err, "failed to remove configured networks")
	}

	return nil
}

func (m *Manager) isDisconnecting() bool {
	m.dmux.Lock()
	defer m.dmux.Unlock()

	return m.disconnecting
}

func (m *Manager) setDisconnecting(d bool) {
	m.dmux.Lock()
	defer m.dmux.Unlock()

	m.disconnecting = d
}

// SignalStrength returns a description of the signal strength
// of the current network, e.g. "Very Good".
func (m *Manager) SignalStrength() string {
	signal := m.supplicant.CurrentSignalStrength()

	return signal.String()
}

// IPAddress returns a dotted decimal string representation of
// the Interface's IP address. If it has no IP, an empty string
// is returned.
func (m *Manager) IPAddress() string {
	ip, err := m.Interface().IPv4Addr()
	if err != nil {
		return ""
	}

	return ip.String()
}

// AvailableNetworks returns the scan results from a Manager's
// supplicant.
func (m *Manager) AvailableNetworks() []supplicant.ScanResult {
	return m.supplicant.ScanResults()
}

// CurrentSSID returns the SSID of the current network.
func (m *Manager) CurrentSSID() string {
	prop := m.supplicant.NetworkProperties()

	ssid, ok := prop["ssid"]
	if !ok {
		return ""
	}

	// Strip the quotes around the SSID that wpa_supplicant adds
	s := ssid.Value().(string)
	if len(s) > 0 {
		return s[1 : len(s)-1]
	}

	return ""
}

// Scan triggers an AP scan with the Managers associated supplicant.
func (m *Manager) Scan() error {
	return m.supplicant.Scan()
}

// State returns the current state of the Manager. Possible states are
// Disconnected, Connected, Connecting.
func (m *Manager) State() State {
	m.smux.Lock()
	defer m.smux.Unlock()

	return m.state
}

// WatchState listens for state changes on the wireless
// interface managed by Manager. It accepts a chan and
// sends the updates over that channel.
func (m *Manager) WatchState(c chan State) {
	l := m.stateBroadcaster.registerListener()
	defer m.stateBroadcaster.unregisterListener(l)

	for {
		c <- <-l.update
	}
}

func (m *Manager) hasIPAddress() bool {
	_, err := m.iface.IPv4Addr()

	return err == nil
}

func (m *Manager) interfaceStateListener() {
	oldState := supplicant.StateUnknown

	// The props channel should not close unless we
	// unregister with the DBus interface
	signal, err := m.supplicant.PropertiesChanged()
	if err != nil {
		log.Printf("Failed to start interface listener: %v", err)
		return
	}

	c := make(chan string)
	go func() {
		for s := range signal {
			props, ok := s.Body[0].(map[string]dbus.Variant)
			if !ok {
				continue
			}

			// If state is not in the map, assume nothing changed.
			newState, ok := props["State"]
			if !ok {
				continue
			}

			// wpa_supplicant puts quotation marks around the state,
			// remove them
			state := newState.String()
			state = state[1 : len(state)-1]

			c <- state
		}
	}()

	for {
		select {
		case newState := <-c:
			m.handleState(newState)
			oldState = newState

		default:
			time.Sleep(1 * time.Second)
			m.handleState(oldState)
		}
	}
}

func (m *Manager) handleState(wpaState string) {
	m.smux.Lock()
	defer m.smux.Unlock()

	var state State

	switch wpaState {
	case supplicant.StateAuthenticating, supplicant.StateAssociating,
		supplicant.StateAssociated, supplicant.State4WayHandshake:

		state = Connecting

	case supplicant.StateDisconnected:
		// If we were trying to connect, and state became disconnected
		// then it is probably because 4-way handshake or EAP failed.
		//
		// Try to ensure that wpa_supplicant stops its association
		// attempt, and that it does not fall back to another network
		// configuration.
		if m.state == Connecting {
			err := m.disconnectAndResetNetworks()
			if err != nil {
				log.Printf("Faild to reset networks after failed association: %v", err)
			}

			state = Failed
		} else {
			state = Disconnected
		}

	case supplicant.StateInactive:
		state = Disconnected

	case supplicant.StateCompleted:
		// If the manager is disconnecting, do not set a
		// false 'connecting' state.
		if m.isDisconnecting() {
			return
		}

		if m.hasIPAddress() {
			state = Connected
		} else {
			state = Connecting
		}

	default:
		// Do not change the state
		return
	}

	if m.state != state {
		m.state = state

		m.stateChanged()

		log.Printf("%s state changed: %s", m.iface.Name, state)
	}
}

func (m *Manager) stateChanged() {
	m.stateBroadcaster.update <- m.state
}

type broadcaster struct {
	update chan State

	mux       sync.Mutex
	listeners map[string]chan State
}

type listener struct {
	update chan State
	id     string
}

func (b *broadcaster) registerListener() *listener {
	// Store the new listener channel
	id := xid.New().String()
	uc := make(chan State)

	b.mux.Lock()
	b.listeners[id] = uc
	b.mux.Unlock()

	lis := listener{
		update: uc,
		id:     id,
	}

	return &lis
}

func (b *broadcaster) unregisterListener(lis *listener) {
	// Remove the listener
	b.mux.Lock()
	defer b.mux.Unlock()

	delete(b.listeners, lis.id)
}

func (b *broadcaster) serve() {
	for {
		update := <-b.update

		// Broadcast update to listeners
		b.mux.Lock()
		for _, v := range b.listeners {
			v <- update
		}
		b.mux.Unlock()
	}
}

func newBroadcaster() *broadcaster {
	b := broadcaster{
		update:    make(chan State, 16),
		listeners: make(map[string]chan State),
	}

	return &b
}
