// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package backend

import (
	"encoding/json"
	"path/filepath"

	scribble "github.com/nanobox-io/golang-scribble"
	"github.com/pkg/errors"

	"gitlab.com/redfield/netctl/api"
)

const (
	defaultDatabasePath = "/storage/services/netctl"
)

type database struct {
	// Driver for network configurations database
	networks *scribble.Driver
}

func createDatabase(path string) (*database, error) {
	if path == "" {
		path = defaultDatabasePath
	}

	// Create networks database
	networks, err := scribble.New(filepath.Join(path, "networks"), nil)
	if err != nil {
		return nil, err
	}

	db := &database{
		networks: networks,
	}

	return db, nil
}

func (db *database) saveNetwork(uuid string, network *api.WirelessNetworkConfiguration) error {
	name := network.GetSsid()

	return db.networks.Write(uuid, name, network)
}

func (db *database) getSavedNetworks(uuid string) ([]*api.WirelessNetworkConfiguration, error) {
	networks := make([]*api.WirelessNetworkConfiguration, 0)

	records, err := db.networks.ReadAll(uuid)
	if err != nil {
		return networks, errors.Wrap(err, "failed to read database")
	}

	for _, r := range records {
		var network api.WirelessNetworkConfiguration
		if err := json.Unmarshal([]byte(r), &network); err != nil {
			return networks, errors.Wrap(err, "failed to unmarshal network configuration")
		}

		networks = append(networks, &network)
	}

	return networks, nil
}
