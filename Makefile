GOPATH ?= $(HOME)/go
PATH := $(GOPATH)/bin:$(PATH)
PKGPATH ?= gitlab.com/redfield/toolstack
DESTDIR ?= /

.PHONY: all
all: bins

.PHONY: bins
bins:
	mkdir -p bin
	go build -o bin/netctl-back cmd/netctl-back/*.go
	go build -o bin/netctl-front cmd/netctl-front/*.go
	go build -o bin/netctl cmd/netctl/*.go

.PHONY: install
install:
	install -d -m 0755 $(DESTDIR)/etc/dbus-1/system.d/
	install -m 0644 configs/com.gitlab.redfield.netctl.wireless.conf $(DESTDIR)/etc/dbus-1/system.d/
	install -d -m 0755 $(DESTDIR)/etc/bash_completion.d
	install -m 0755 configs/netctl.bash_completion.sh $(DESTDIR)/etc/bash_completion.d/

.PHONY: clean
clean:
	rm -rf bin/

.PHONY: deps
deps:
	go get ./...


.PHONY: fmt
fmt:
	find api/ cmd/ pkg/ -pname '*.go' | xargs gofmt -w -s

.PHONY: vendor
vendor:
	GO111MODULE=on go mod vendor

api/netctl.pb.go: api/netctl.proto
	protoc -I api --go_out=plugins=grpc:api api/netctl.proto

.PHONY: proto
proto: api/netctl.pb.go

.PHONY: check
check: golint all test
	DESTDIR=/tmp make install

.PHONY: test
test:
	go test -v ./pkg/...

.PHONY: golint
golint:
	golangci-lint --verbose run --enable-all -Dgochecknoglobals -Dgochecknoinits -Dlll -Dfunlen
