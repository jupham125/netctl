// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"fmt"
	"os"
	"strings"
	"text/tabwriter"

	"github.com/pkg/errors"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	"gitlab.com/redfield/netctl/api"
)

var (
	// Global backend and frontend clients
	bc  *api.BackendClient
	nfc *api.FrontendClient
	nf  *api.FrontendMetaData

	// Tabwriter for output
	writer = tabwriter.NewWriter(os.Stdout, 8, 8, 2, '\t', tabwriter.AlignRight)
)

const (
	bashCompletion = `__netctl_custom_func() {
    case ${last_command} in
	netctl_wifi_connect)
	    __netctl_wifi_network_nouns
	    return
	    ;;
	*)
	    ;;
    esac
}

__netctl_wifi_network_nouns() {
    local network_nouns
    if network_nouns=$(netctl wifi list --no-header 2>/dev/null); then
	out=($(echo "${network_nouns}" | awk '{print $1}'))
        COMPREPLY=( $( compgen -W "${out[*]}" -- "$cur" ) )
    fi
}
`
)

var (
	rootCmd = &cobra.Command{
		Use:                    "netctl",
		Short:                  "Redfield netctl client",
		SilenceErrors:          true,
		SilenceUsage:           true,
		BashCompletionFunction: bashCompletion,
	}

	bashCompletionCmd = &cobra.Command{
		Use:    "completion",
		Short:  "Generate bash completion scripts",
		Hidden: true,
		RunE: func(cmd *cobra.Command, args []string) error {
			err := rootCmd.GenBashCompletion(os.Stdout)
			if err != nil {
				return errors.Wrap(err, "failed to generate bash completion scripts")
			}

			return nil
		},
	}

	listFrontendsCmd = &cobra.Command{
		Use:   "list",
		Short: "List registered frontends",
		PreRunE: func(cmd *cobra.Command, args []string) error {
			return preRun(cmd)
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			frontends, err := bc.GetAllFrontends()
			if err != nil {
				return errors.Wrap(err, "failed to get registered frontends")
			}

			header := formatTableHeader("Type", "UUID")
			fmt.Fprintln(writer, header)

			for _, frontend := range frontends {
				line := formatTableLine(frontend.Type.String(), frontend.Uuid)
				fmt.Fprintln(writer, line)
			}

			writer.Flush()

			return nil
		},
	}

	wifiCmd = &cobra.Command{
		Use:   "wifi",
		Short: "Perform wireless-specific commands",
	}

	wifiListCmd = &cobra.Command{
		Use:   "list",
		Short: "List available wireless networks",
		PreRunE: func(cmd *cobra.Command, args []string) error {
			return wifiPreRun(cmd)
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			props, err := nfc.WirelessGetProperties()
			if err != nil {
				return errors.Wrap(err, "failed to get wireless properties")
			}

			if !viper.GetBool("no-header") {
				header := formatTableHeader("SSID", "Security", "Signal")
				fmt.Fprintln(writer, header)
			}

			for _, ap := range props.GetAccessPoints() {
				// Do not show hidden networks.
				if strings.TrimSpace(ap.Ssid) == "" {
					continue
				}

				line := formatTableLine(ap.Ssid, ap.KeyMgmt, ap.SignalStrength)
				fmt.Fprintln(writer, line)
			}

			writer.Flush()

			return nil
		},
	}

	wifiConnectCmd = &cobra.Command{
		Use:   "connect",
		Short: "Connect to a wireless network",
		Args:  cobra.RangeArgs(1, 2),
		PreRunE: func(cmd *cobra.Command, args []string) error {
			return wifiPreRun(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {
			// Args will be length 1 or 2
			if len(args) == 1 {
				// First, check if the network name matches any saved
				// networks. If not, assume this an attempt to connect
				// to a hidden network.
				if connectSavedNetwork(args[0]) {
					return
				}

				connectWirelessOpen(args[0], viper.GetBool("hidden"))
			} else {
				connectWirelessPSK(args[0], args[1], viper.GetBool("hidden"))
			}
		},
	}

	wifiDisconnectCmd = &cobra.Command{
		Use:   "disconnect",
		Short: "Disconnect from the current network",
		PreRunE: func(cmd *cobra.Command, args []string) error {
			return wifiPreRun(cmd)
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			return nfc.WirelessDisconnect()
		},
	}

	wifiStatusCmd = &cobra.Command{
		Use:   "status",
		Short: "Status of wireless frontend",
		PreRunE: func(cmd *cobra.Command, args []string) error {
			return wifiPreRun(cmd)
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			props, err := nfc.WirelessGetProperties()
			if err != nil {
				return errors.Wrap(err, "failed to get wireless properties")
			}

			columns := []string{"State", "SSID", "IP Address", "Signal"}
			items := []string{props.State.String(), props.Ssid, props.IpAddress, props.SignalStrength}

			if props.State == api.WirelessState_PORTAL {
				columns = append(columns, "Portal URL")
				items = append(items, props.PortalUrl)
			}

			header := formatTableHeader(columns...)
			line := formatTableLine(items...)

			fmt.Fprintln(writer, header)
			fmt.Fprintln(writer, line)

			writer.Flush()

			return nil
		},
	}
)

func formatTableHeader(columns ...string) string {
	header := strings.Join(columns, "\t")

	divider := make([]string, len(columns))
	for i, col := range columns {
		divider[i] = strings.Repeat("-", len(col))
	}

	return fmt.Sprintf("%s\n%s", header, strings.Join(divider, "\t"))
}

func formatTableLine(items ...string) string {
	line := strings.Repeat("%s\t", len(items))

	args := make([]interface{}, len(items))

	for i, item := range items {
		if strings.TrimSpace(item) == "" {
			item = "-"
		}

		args[i] = item
	}

	return fmt.Sprintf(line, args...)
}

func preRun(cmd *cobra.Command) error {
	var err error

	if err = viper.BindPFlags(cmd.Flags()); err != nil {
		return errors.Wrap(err, "failed to bind flags")
	}

	// Assigning to global var
	bc, err = newBackendClient(viper.GetString("address"))
	if err != nil {
		return errors.Wrap(err, "failed to create backend client")
	}

	return nil
}

func wifiPreRun(cmd *cobra.Command) error {
	var err error

	if err := preRun(cmd); err != nil {
		return err
	}

	nf, err = bc.GetFrontend(viper.GetString("uuid"))
	if err != nil {
		return errors.Wrap(err, "failed to find frontend")
	}

	// Assign global variable 'nfc'
	nfc, err = newFrontendClient(nf)
	if err != nil {
		return errors.Wrap(err, "failed to create frontend client")
	}

	return nil
}

func init() {
	viper.SetEnvPrefix("NETCTL")
	viper.SetEnvKeyReplacer(strings.NewReplacer("-", "_"))
	viper.AutomaticEnv()

	// Root command
	rootCmd.PersistentFlags().String("address", "tcp://:50051", "Server address (tcp://host:port or unix:///path)")

	wifiConnectCmd.PersistentFlags().Bool("hidden", false, "Connect to network with hidden SSID")

	rootCmd.AddCommand(listFrontendsCmd, wifiCmd, bashCompletionCmd)

	// Wifi command
	wifiCmd.PersistentFlags().String("uuid", "", "UUID for wireless frontend")

	wifiListCmd.PersistentFlags().Bool("no-header", false, "Do not display WiFi list header")

	wifiCmd.AddCommand(wifiListCmd, wifiConnectCmd, wifiDisconnectCmd, wifiStatusCmd)
}

func main() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println("Error: ", err)
		os.Exit(-1)
	}
}
