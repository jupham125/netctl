// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"fmt"
	"time"

	"github.com/pkg/errors"

	"gitlab.com/redfield/netctl/api"
	"gitlab.com/redfield/netctl/pkg/supplicant"
)

func newBackendClient(addr string) (*api.BackendClient, error) {
	ti, err := api.ParseTransport(addr)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create transport")
	}

	cc, err := ti.ClientConn()
	if err != nil {
		return nil, errors.Wrap(err, "failed to create client conn")
	}

	bc := api.NewBackendClient(cc)
	bc.SetTimeout(10 * time.Second)

	return bc, nil
}

func newFrontendClient(nf *api.FrontendMetaData) (*api.FrontendClient, error) {
	ti := &api.Transport{Info: nf.TransportInfo}

	cc, err := ti.ClientConn()
	if err != nil {
		return nil, errors.Wrap(err, "failed to create client conn")
	}

	fc := api.NewFrontendClient(cc)
	fc.SetTimeout(10 * time.Second)

	return fc, nil
}

func connectWirelessOpen(ssid string, hidden bool) {
	conf := &api.WirelessNetworkConfiguration{
		Ssid:    ssid,
		KeyMgmt: supplicant.AuthOpen,
	}
	if hidden {
		conf.ScanSsid = 1
	}

	state, err := nfc.WirelessConnect(conf)
	if err != nil {
		fmt.Printf("Failed to connect to '%s': %v\n", ssid, err)
		return
	}

	switch state {
	case api.WirelessState_CONNECTED:
		fmt.Printf("Successfully connected to '%s'.\n", ssid)
	case api.WirelessState_FAILED:
		fmt.Printf("Failed to connect to '%s': unknown error\n.", ssid)
	case api.WirelessState_PORTAL:
		fmt.Println("Captive portal detected. Run `netctl wifi status` for more info.")
	}
}

func connectWirelessPSK(ssid, psk string, hidden bool) {
	conf := &api.WirelessNetworkConfiguration{
		Ssid:    ssid,
		KeyMgmt: supplicant.AuthPSK,
		Psk:     psk,
	}
	if hidden {
		conf.ScanSsid = 1
	}

	state, err := nfc.WirelessConnect(conf)
	if err != nil {
		fmt.Printf("Failed to connect to '%s': %v\n", ssid, err)
		return
	}

	switch state {
	case api.WirelessState_CONNECTED:
		fmt.Printf("Successfully connected to '%s'.\n", ssid)
	case api.WirelessState_FAILED:
		fmt.Printf("Failed to connect to '%s': incorrect password?\n", ssid)
	case api.WirelessState_PORTAL:
		fmt.Println("Captive portal detected. Run `netctl wifi status` for more info.")
	}
}

// connectSavedNetwork tries to make a connection using a saved
// network configuration. Returns true if a config was found and
// a connection attempt is made, and false if no config was found.
func connectSavedNetwork(ssid string) bool {
	cfgs, err := bc.GetSavedNetworks(nf)
	if err != nil {
		fmt.Printf("Error: failed to load saved networks: %v\n", err)

		return false
	}

	var conf *api.WirelessNetworkConfiguration

	for _, cfg := range cfgs {
		if cfg.Ssid == ssid {
			conf = cfg
			break
		}
	}

	if conf == nil {
		return false
	}

	state, err := nfc.WirelessConnect(conf)
	if err != nil {
		fmt.Printf("Failed to connect to '%s': %v\n", ssid, err)
		return true
	}

	switch state {
	case api.WirelessState_CONNECTED:
		fmt.Printf("Successfully connected to '%s'.\n", ssid)
	case api.WirelessState_FAILED:
		fmt.Printf("Failed to connect to '%s': incorrect password?\n", ssid)
	case api.WirelessState_PORTAL:
		fmt.Println("Captive portal detected. Run `netctl wifi status` for more info.")
	}

	return true
}
