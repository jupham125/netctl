// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package api

import (
	"context"
	"time"

	"google.golang.org/grpc"
)

// FrontendClient provides a client for the netctl_front service.
type FrontendClient struct {
	c NetctlFrontClient

	// Keep track so it can be closed later.
	cc *grpc.ClientConn

	// Timeout for RPC calls.
	timeout time.Duration
}

// NewFrontendClient returns a new netctl_front client.
func NewFrontendClient(cc *grpc.ClientConn) *FrontendClient {
	fc := &FrontendClient{
		c:       NewNetctlFrontClient(cc),
		cc:      cc,
		timeout: 30 * time.Second,
	}

	return fc
}

// SetTimeout sets the timeout used to call non-streaming RPCs.
func (fc *FrontendClient) SetTimeout(timeout time.Duration) {
	fc.timeout = timeout
}

// Close closes the frontend client connection.
func (fc *FrontendClient) Close() error {
	return fc.cc.Close()
}

// Notify sends a notification to a frontend.
func (fc *FrontendClient) Notify(n *Notification) error {
	r := &NotifyRequest{
		Notification: n,
	}

	ctx, cancel := context.WithTimeout(context.Background(), fc.timeout)
	defer cancel()

	_, err := fc.c.Notify(ctx, r)

	return err
}

// WirelessConnect connects the frontend to a wireless network with a given
// network configuration.
func (fc *FrontendClient) WirelessConnect(conf *WirelessNetworkConfiguration) (WirelessState, error) {
	r := &WirelessConnectRequest{
		Config: conf,
	}

	ctx, cancel := context.WithTimeout(context.Background(), fc.timeout)
	defer cancel()

	reply, err := fc.c.WirelessConnect(ctx, r)

	if err != nil {
		return WirelessState_UNKNOWN, err
	}

	return reply.State, nil
}

// WirelessDisconnect disconnects the frontend from its wireless network.
func (fc *FrontendClient) WirelessDisconnect() error {
	r := &WirelessDisconnectRequest{}

	ctx, cancel := context.WithTimeout(context.Background(), fc.timeout)
	defer cancel()

	_, err := fc.c.WirelessDisconnect(ctx, r)

	return err
}

// WirelessPropertiesStream represents a stream of wireless properties.
type WirelessPropertiesStream struct {
	NetctlFront_WirelessMonitorPropertiesClient
}

// WirelessMonitorProperties registers a properties monitor and returns a stream.
func (fc *FrontendClient) WirelessMonitorProperties(ctx context.Context) (*WirelessPropertiesStream, error) {
	r := &WirelessMonitorPropertiesRequest{}

	stream, err := fc.c.WirelessMonitorProperties(ctx, r)
	if err != nil {
		return nil, err
	}

	return &WirelessPropertiesStream{stream}, nil
}

// WirelessGetProperties returns the properties of the wireless frontend.
func (fc *FrontendClient) WirelessGetProperties() (*WirelessProperties, error) {
	r := &WirelessGetPropertiesRequest{}

	ctx, cancel := context.WithTimeout(context.Background(), fc.timeout)
	defer cancel()

	reply, err := fc.c.WirelessGetProperties(ctx, r)

	return reply.GetProps(), err
}
